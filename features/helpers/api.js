'use strict';

const supertest = require('supertest');
const request = supertest('https://api.jsonbin.io/v3');

exports.binID = "";
exports.cachedResponse = "";

exports.apiCall = (type, route, body) => {
    let headerContentTypeJson = 'application/json';
    let headerKey = '$2b$10$yRLrySpde12UhYttYQa8XOVcfclgSK64XjvcDwdSg8oVLIG6kj9mm'
    //^^maybe this could be in a .env file but for the sake of being able to run these in gitlab I will leave it here for now

    switch (type) {
        case "POST":
            return request.post(route)
                .set('Content-Type', headerContentTypeJson)
                .set('X-Master-Key', headerKey)
                .send({
                    body
                }).then(response => {
                    exports.binID = response.body.metadata.id
                    exports.cachedResponse = response
                })

        case "GET":
            return request.get(route + "/" + this.binID)
                .set('X-Master-Key', headerKey)
                .send()
                .then(response => {
                    exports.cachedResponse = response
                })

        case "UPDATE":
            return request.put(route + "/" + this.binID)
                .set('X-Master-Key', headerKey)
                .set('Content-Type', headerContentTypeJson)
                .send(
                    {
                        body
                    }
                )
                .then(response => {
                    exports.cachedResponse = response
                })

        case "DELETE":
            return request.delete(route + "/" + this.binID)
                .set('X-Master-Key', headerKey)
                .set('Content-Type', headerContentTypeJson)
                .send()
                .then(response => {
                    exports.cachedResponse = response
                })
    }
}