Feature: CRUD API calls

    Making CRUD API calls

    Scenario: Making a POST call should create a BIN
        Given I make a "POST" call to the "/b" endpoint
        Then Status code is 200

    Scenario: Reading a bin that has just been created
        Given I make a "POST" call to the "/b" endpoint with body
        ```
        {
        "name": "arthur's bin"
        }
        ```
        When I make a "GET" call to the "/b" endpoint
        Then Response body contains string "arthur's bin"

    Scenario: Updating a bin that has just been created
        Given I make a "POST" call to the "/b" endpoint with body
        ```
        {
        "name": "arthur's bin"
        }
        ```
        And I make a "GET" call to the "/b" endpoint
        Then Response body contains string "arthur's bin"
        When I make a "UPDATE" call to the "/b" endpoint with body
        ```
        {
        "name": "arthur's updated bin"
        }
        ```
        Then Response body does not contain string "arthur's bin"
        And Response body contains string "arthur's updated bin"

    Scenario: Deleting a bin that has just been created
        Given I make a "POST" call to the "/b" endpoint
        And I make a "GET" call to the "/b" endpoint
        Then Status code is 200
        When I make a "DELETE" call to the "/b" endpoint
        Then Response body contains string "Bin deleted successfully"
        When I make a "GET" call to the "/b" endpoint
        And I make a "GET" call to the "/b" endpoint
        Then Status code is 404

#for some reason at line 42 jsonbin still reports that the bin is present, only when then second GET takes place it realizes that it's gone
#probably there would've been a retry workaround given enough time