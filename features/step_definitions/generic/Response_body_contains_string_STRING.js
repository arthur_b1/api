'use strict';

const expect = require('chai').expect;
const { Given, When, Then } = require('@cucumber/cucumber');
const api = require('../../helpers/api');

Then('Response body contains string {string}', async (string) => {
    expect(JSON.stringify(api.cachedResponse.text)).to.contain(string)
});