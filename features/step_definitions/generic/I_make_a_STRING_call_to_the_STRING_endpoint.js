'use strict';

const { Given, When, Then } = require('@cucumber/cucumber');
const api = require('../../helpers/api');

Given('I make a {string} call to the {string} endpoint', async (string1, string2) => {
    await api.apiCall(string1, string2, {})
});

Given('I make a {string} call to the {string} endpoint with body', async (string1, string2, bodyJsonString) => {
    await api.apiCall(string1, string2, JSON.parse(bodyJsonString))
});