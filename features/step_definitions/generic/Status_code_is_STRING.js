'use strict';

const { Given, When, Then } = require('@cucumber/cucumber');
const api = require('../../helpers/api');
const expect = require('chai').expect;

Then('Status code is {int}', async (string) => {
    expect(api.cachedResponse.statusCode).to.be.equal(string)
});