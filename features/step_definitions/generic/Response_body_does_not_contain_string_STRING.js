'use strict';

const expect = require('chai').expect;
const { Given, When, Then } = require('@cucumber/cucumber');
const api = require('../../helpers/api');

Then('Response body does not contain string {string}', async (string) => {
    expect(JSON.stringify(api.cachedResponse.text)).to.not.contain(string)
});